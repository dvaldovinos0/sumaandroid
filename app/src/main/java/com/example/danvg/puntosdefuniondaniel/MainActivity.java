package com.example.danvg.puntosdefuniondaniel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText et1, et2, et3, et4, et5, et6, et7, et8, et9, et10, et11, et12, et13, et14, et15, et16, et17, et18, et19, et20, et21, et22, et23;
    double _PFSA;
    double _fA;
    double _pfa;
    double hh;
    double lc;
    double horas;
    double dias;
    int nDesa;
    double meses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et1 = (EditText) findViewById(R.id.editText);
        et2 = (EditText) findViewById(R.id.editText2);
        et3 = (EditText) findViewById(R.id.editText3);
        et4 = (EditText) findViewById(R.id.editText4);
        et5 = (EditText) findViewById(R.id.editText5);
        et6 = (EditText) findViewById(R.id.editText6);
        et7 = (EditText) findViewById(R.id.editText7);
        et8 = (EditText) findViewById(R.id.editText8);
        et9 = (EditText) findViewById(R.id.editText9);
        et10 = (EditText) findViewById(R.id.editText10);
        et11 = (EditText) findViewById(R.id.editText11);
        et12 = (EditText) findViewById(R.id.editText12);
        et13 = (EditText) findViewById(R.id.editText13);
        et14 = (EditText) findViewById(R.id.editText14);
        et15 = (EditText) findViewById(R.id.editText15);
        et16 = (EditText) findViewById(R.id.editText16);
        et17 = (EditText) findViewById(R.id.editText17);
        et18 = (EditText) findViewById(R.id.editText18);
        et19 = (EditText) findViewById(R.id.editText19);
        et20 = (EditText) findViewById(R.id.editText20);
        et21 = (EditText) findViewById(R.id.editText21);
        et22 = (EditText) findViewById(R.id.editText22);
        et23 = (EditText) findViewById(R.id.editText24);

        Button btnBaja = (Button) findViewById(R.id.button);
        Button btnMedia = (Button) findViewById(R.id.button2);
        Button btnAlta = (Button) findViewById(R.id.button3);
        Button btnCalcular = (Button) findViewById(R.id.button4);
        Button btnCalcularPFA = (Button) findViewById(R.id.button5);
        Button btnEnsamblador = (Button) findViewById(R.id.button6);
        Button btnCobol = (Button) findViewById(R.id.button7);
        Button btnCuarta = (Button) findViewById(R.id.button8);
        Button btnDura = (Button) findViewById(R.id.button9);
        Button btnpresu = (Button) findViewById(R.id.button10);


        final TextView PFSA = (TextView) findViewById(R.id.textView12);
        final TextView FA = (TextView) findViewById(R.id.textView32);
        final TextView PFA = (TextView) findViewById(R.id.textView36);
        final TextView HH = (TextView) findViewById(R.id.textView40);
        final TextView LC = (TextView) findViewById(R.id.textView43);
        final TextView horis = (TextView) findViewById(R.id.textView50);
        final TextView diis = (TextView) findViewById(R.id.textView51);
        final TextView duracion = (TextView) findViewById(R.id.textView52);
        final TextView PRESU = (TextView) findViewById(R.id.textView58);

        btnBaja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int val1 = Integer.valueOf(et1.getText().toString());
                int res1 = val1*3;
                int val2 = Integer.valueOf(et2.getText().toString());
                int res2 = val2*4;
                int val3 = Integer.valueOf(et3.getText().toString());
                int res3 = val3*3;
                int val4 = Integer.valueOf(et4.getText().toString());
                int res4 = val4*7;
                int val5 = Integer.valueOf(et5.getText().toString());
                int res5 = val5*5;

                _PFSA = res1 + res2 + res3 + res4 + res5;

                PFSA.setText(""+ _PFSA);
            }
        });



        btnMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int val1 = Integer.valueOf(et1.getText().toString());
                int res1 = val1*4;
                int val2 = Integer.valueOf(et2.getText().toString());
                int res2 = val2*5;
                int val3 = Integer.valueOf(et3.getText().toString());
                int res3 = val3*4;
                int val4 = Integer.valueOf(et4.getText().toString());
                int res4 = val4*10;
                int val5 = Integer.valueOf(et5.getText().toString());
                int res5 = val5*7;

                _PFSA = res1 + res2 + res3 + res4 + res5;

                PFSA.setText(""+ _PFSA);
            }
        });


        btnAlta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int val1 = Integer.valueOf(et1.getText().toString());
                int res1 = val1*6;
                int val2 = Integer.valueOf(et2.getText().toString());
                int res2 = val2*7;
                int val3 = Integer.valueOf(et3.getText().toString());
                int res3 = val3*6;
                int val4 = Integer.valueOf(et4.getText().toString());
                int res4 = val4*15;
                int val5 = Integer.valueOf(et5.getText().toString());
                int res5 = val5*10;

                _PFSA = res1 + res2 + res3 + res4 + res5;

                PFSA.setText(""+ _PFSA);
            }
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int val1 = Integer.valueOf(et6.getText().toString());
                int val2 = Integer.valueOf(et7.getText().toString());
                int val3 = Integer.valueOf(et8.getText().toString());
                int val4 = Integer.valueOf(et9.getText().toString());
                int val5 = Integer.valueOf(et10.getText().toString());
                int val6 = Integer.valueOf(et11.getText().toString());
                int val7 = Integer.valueOf(et12.getText().toString());
                int val8 = Integer.valueOf(et13.getText().toString());
                int val9 = Integer.valueOf(et14.getText().toString());
                int val10 = Integer.valueOf(et14.getText().toString());
                int val11 = Integer.valueOf(et15.getText().toString());
                int val12 = Integer.valueOf(et16.getText().toString());
                int val13 = Integer.valueOf(et17.getText().toString());
                int val14 = Integer.valueOf(et19.getText().toString());


                _fA = val1 + val2 + val3 + val4 + val5 + val6 + val7 + val8 + val9 + val10 + val11 + val12 + val13 + val14;

                FA.setText(""+_fA);
            }
        });

        btnCalcularPFA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // PFA = PFSA*[0.65+(0.01*FA)]

                 _pfa = (_PFSA*(0.65+(0.01*_fA)));

                PFA.setText(""+_pfa);
            }
        });

        btnEnsamblador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               //Horas – Hombre = PFA * Horas PF promedio;

                hh = _pfa*25;
                HH.setText(""+hh);
                lc = _pfa*300;
                LC.setText(""+lc + " LC");
            }
        });
        btnCobol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Horas – Hombre = PFA * Horas PF promedio;

                hh = _pfa*15;
                HH.setText(""+hh);

                lc = _pfa*100;
                LC.setText(""+lc + " LC");
            }
        });
        btnCuarta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Horas – Hombre = PFA * Horas PF promedio;

                hh = _pfa*8;
                HH.setText(""+hh+ " H-H");
                lc = _pfa*20;
                LC.setText(""+lc + " LC");
            }
        });

        btnDura.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nDesa = Integer.valueOf(et20.getText().toString());
                int hDef = Integer.valueOf(et21.getText().toString());

                horas=hh/nDesa;
                dias=horas/hDef;

                horis.setText(""+horas + " Horas");
                diis.setText(""+dias + " Dias");
                meses = dias/20;
                duracion.setText(""+dias + "/20 = "+meses + " meses para desarrollar el software  de lunes a viernes. " + hDef + " horas diarias con " + nDesa + " desarrolladores (ESTIMACIÓN de duración del proyecto)");
            }
        });

        btnpresu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int sueldo = Integer.valueOf(et22.getText().toString());
                int costos = Integer.valueOf(et23.getText().toString());

                double costo=(nDesa*meses*sueldo)+costos;

                PRESU.setText("("+ nDesa +"*" +meses+"*"+sueldo+")+"+costos+" = " + costo);
            }
        });
    }
}
